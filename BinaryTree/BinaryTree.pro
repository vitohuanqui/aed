#-------------------------------------------------
#
# Project created by QtCreator 2014-09-16T10:49:55
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = BinaryTree
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11
TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    BinTree.h \
    Trait.h \
    Iterator.h
