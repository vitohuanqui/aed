
#ifndef TRAIT_H
#define TRAIT_H

template<typename T>
struct Min{bool operator ()(T a,T b){return a<b;}};

template<typename T>
struct Max{bool operator ()(T a,T b){return a>b;}};

template<typename T>
struct Equal{bool operator ()(T a,T b){return a==b;}};

struct traitintmin
{
    typedef int T;
    typedef Min<T> C;
};

struct traitintmax
{
    typedef int T;
    typedef Max<T> C;
};

struct traitintequal
{
    typedef int T;
    typedef Equal<T> C;
};



#endif // TRAIT_H
