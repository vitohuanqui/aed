#include "BinTree.h"
#include <iostream>
#include "Trait.h"
#include "Iterator.h"

using namespace std;

int main(int argc, char *argv[])
{
    CBinTree<traitintmin> arbol;
    for (int i=0;i<100;i++){
        arbol.insert(i);
    }
    CBinTree<traitintmin>::iterin it;
    for (it=arbol.begin_in();it!=arbol.end_in();it++){
        cout<<*it;
    }
    cout<<arbol.root->data<<endl;
    cout<<arbol.root->nodes[0]->data<<"-"<<arbol.root->nodes[1]->data<<endl;
    cout<<arbol.root->nodes[0]->nodes[0]->data<<"-"<<arbol.root->nodes[0]->nodes[1]->data<<"-"<<arbol.root->nodes[1]->nodes[1]->data<<endl;

}
