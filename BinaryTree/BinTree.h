#ifndef BINTREE_H
#define BINTREE_H

#include <math.h>
#include <iostream>
#include "Iterator.h"
#include <stack>

using namespace std;

template <class T>
struct CNode
{
    T data;
    CNode* nodes[2];
    CNode(T x){
        data = x;
        nodes[0]=nodes[1]=NULL;
    }
    int height=0;
    int fac_equi=0;
};

template<typename T>
struct iteratorin
{
  stack< pair<CNode<T>*,int> > pila;
  iteratorin(){}

  iteratorin(stack<pair<CNode<T>*,int>> oa){
    pila=oa;
  }
  CNode<T>*& get_node(){
      return (pila.top().first);
  }

  T operator * ()
  {
    return ((pila.top()).first)->data;
  }
  iteratorin operator ++(int)
  {
    iteratorin tmp;
    tmp=*this;
    func();
    return tmp;
  }
  iteratorin operator ++()
  {
    func();
    return *this;
  }
  iteratorin operator =(iteratorin oa)
  {
    pila=oa.pila;
    return *this;
  }
  bool operator != (iteratorin oa)
  {
    return pila!=oa.pila;
  }
  void func()
  {
    while(1)
    {
      if(pila.empty())
          return;
      switch((pila.top()).second)
      {
        case 0:
          if(!((pila.top()).first)->nodes[0])
            (pila.top()).second+=1;
          else
            pila.push(pair<CNode<T>*,int>((((pila.top()).first)->nodes[0]),0));
          break;
        case 1:
          (pila.top()).second++;
          return;
        case 2:
          if(!((pila.top()).first)->nodes[1])
            (pila.top()).second+=1;
          else
            pila.push(pair<CNode<T>*,int>((((pila.top()).first)->nodes[1]),0));
          break;
        default:
          pila.pop();
          (pila.top()).second+=1;
      }
    }
  }
};

template<typename T>
struct iteratorpre
{
  iteratorpre(){}

  iteratorpre(stack< pair<CNode<T>*,int> > oa){
    pila=oa;
  }
  stack< pair<CNode<T>*,int> > pila;
  iteratorpre operator ++(int)
  {
    iteratorpre tmp;
    tmp=*this;
    func();
    return tmp;
  }
  T operator * ()
  {
    return ((pila.top()).first)->data;
  }
  iteratorpre operator ++()
  {
    func();
    return *this;
  }
  iteratorpre operator =(iteratorpre oa)
  {
    pila=oa.pila;
    return *this;
  }
  bool operator != (iteratorpre oa)
  {
    return pila!=oa.pila;
  }
  void func()
  {
    while(1)
    {
      if(pila.empty())
          return;
      switch((pila.top()).second)
      {
        case 1:
          if(!((pila.top()).first)->nodes[0])
            (pila.top()).second++;
          else
            pila.push(pair<CNode<T>*,int>((((pila.top()).first)->nodes[0]),0));
          break;
        case 0:
          (pila.top()).second++;
          return;
        case 2:
          if(!((pila.top()).first)->nodes[1])
            (pila.top()).second++;

          else
            pila.push(pair<CNode<T>*,int>((((pila.top()).first)->nodes[1]),0));
          break;
        default:
          pila.pop();
          (pila.top()).second++;
      }
    }
  }
};

template<typename T>
struct iteratorpost
{
  iteratorpost(){}
  iteratorpost(stack< pair<CNode<T>*,int> > oa){
    pila=oa;
  }
  T operator * ()
  {
    return ((pila.top()).first)->data;
  }
  stack< pair<CNode<T>*,int> > pila;
  iteratorpost operator ++(int)
  {
    iteratorpost tmp;
    tmp=*this;
    func();
    return tmp;
  }
  iteratorpost operator ++()
  {
    func();
    return *this;
  }
  iteratorpost operator =(iteratorpost oa)
  {
    pila=oa.pila;
    return *this;
  }
  bool operator != (iteratorpost oa)
  {
    return pila!=oa.pila;
  }
  void func()
  {
    while(1)
    {
      if(pila.empty())
          return;
      switch((pila.top()).second)
      {
        case 0:
          if(!((pila.top()).first)->nodes[0])
            (pila.top()).second++;
          else
            pila.push(pair<CNode<T>*,int>((((pila.top()).first)->nodes[0]),0));
          break;
        case 2:
          (pila.top()).second++;
          return;
        case 1:
          if(!((pila.top()).first)->nodes[1])
            (pila.top()).second++;
          else
            pila.push(pair<CNode<T>*,int>((((pila.top()).first)->nodes[1]),0));
          break;
        default:
          pila.pop();
          (pila.top()).second++;
      }
    }
  }
};


template <class Tr>
class CBinTree
{
public:
    typedef typename Tr::T T;
    typedef typename Tr::C C;
    typedef iteratorin<T> iterin;
    typedef iteratorpre<T> iterpre;
    typedef iteratorpost<T> iterpos;
    typedef CNode<T> Node;
    Node* root = 0;
    C m_c;

    iterin begin_in(){
        stack<pair<Node*,int>> pila;
        pila.push(pair<Node*,int>(root,0));
        iterin it(pila);
        it++;
        return it;
    }
    iterin end_in(){
        return iterin();
    }
    iterpre begin_pre(){
        stack< pair<Node*,int> > pila;
        pila.push(pair<Node*,int>(root,0));
        iterpre it(pila);
        it++;
        return it;
    }
    iterpre end_pre(){
        return iterpre();
    }
    iterpos begin_pos(){
        stack< pair<Node*,int> > pila;
        pila.push(pair<Node*,int>(root,0));
        iterpos it(pila);
        it++;
        return it;
    }
    iterpos end_pos(){
        return iterpos();
    }

    bool find(T x,Node** &p,stack<Node**> &all_nodes){
        for(p = &root; (*p) && (*p)->data!=x ; p=&((*p)->nodes[m_c((*p)->data,x)])){
            all_nodes.push(p);
        }
        return (!!(*p));

    }
    bool balanced_tree(Node** p,bool &camino){
        if((*p)->nodes[0] || (*p)->nodes[1]){
            int alt1 = -1;
            int alt2 = -1;
            if((*p)->nodes[0])
                alt1 = (*p)->nodes[0]->height;
            if((*p)->nodes[1])
                alt2 = (*p)->nodes[1]->height;
            if(alt1-alt2>0){
                camino= 0;
                return alt1-alt2-2;
            }
            camino = 1;
            return alt1-alt2+2;
        }
        return 1;
    }
    void rotation_simple(Node** p, bool camino){
        Node* tmp = (*p);
        (*p) = (*p)->nodes[camino];
        if( (*p)->nodes[!camino] ){
          tmp->nodes[camino] = (*p)->nodes[!camino];
        }
        else{
          tmp->nodes[camino] = NULL;
        }
        (*p)->nodes[!camino] = tmp;
        int h1=-1,h2=-1;
        if (tmp->nodes[0])
            h1 = tmp->nodes[0]->height;
        if (tmp->nodes[1])
            h2 = tmp->nodes[1]->height;
        tmp->height = (max(h1,h2)) +1;
        tmp->fac_equi = h1 - h2;
        h1=h2=-1;
        if ((*p)->nodes[0])
            h1 = (*p)->nodes[0]->height;
        if ((*p)->nodes[1])
            h2 = (*p)->nodes[1]->height;
        (*p)->height = (max(h1,h2)) +1;
        (*p)->fac_equi = h1 - h2;
    }

    void balance(Node** p){
        bool camino;
        if (balanced_tree(p,camino)) return;
        if(((*p)->nodes[camino])->nodes[camino])
        {
            if(!((*p)->nodes[camino])->nodes[!camino] or
               ((*p)->nodes[camino])->nodes[camino]->height >=
               ((*p)->nodes[camino])->nodes[!camino]->height){
              rotation_simple(p,camino);
              return;
          }
        }
        Node** q = &(*p)->nodes[camino];
        rotation_simple(q,!camino);
        rotation_simple(p,camino);
        return;
    }
    /*bool insert_avl(T x,Node ** p){
        bool inserto=0;
        if ((*p) && (*p)->data!=x)
            inserto=insert_avl(x,&((*p)->nodes[C((*p)->data,x)]));
        else if (*p)
            return 0;
        else{
            (*p) = new Node(x);
            return 1;
        }
        if (inserto){
            int altura1=-1,altura2=-1;
            if ((*p)->nodes[0])
                altura1 = (*p)->nodes[0]->altura;
            if ((*p)->nodes[1])
                altura2 = (*p)->nodes[1]->altura;
            (*p)->altura = (max(altura1,altura2)) +1;
            (*p)->fac_equi = altura1 - altura2;
            if ((*p)->fac_equi>=2)
            {
                if ((*p)->fac_equi == 2)
                {
                    if ((*p)->nodes[0]){
                        if ((*p)->nodes[0]->fac_equi == 1)
                            RL(p);
                    }
                    SL(p);
                }
                else
                {
                    if ((*p)->nodes[1]){
                        if ((*p)->nodes[1]->fac_equi == -1)
                            LR(p);
                    }
                    SR(p);
                }
            }

        }
        return inserto;
    }
    void SR(Node** p){
        //rotacion simple
        Node** tmp = p;
        if ((*p)->fac_equi>0)
        {
            (*p) = (*p)->nodes[0];
            (*p)->nodes[1] = tmp;
        }
        else
        {
            (*p) = (*p)->nodes[1];
            (*p)->nodes[0] = tmp;
        }
        (*tmp)->height = max(((*tmp)->nodes[0],(*tmp)->nodes[1]))+1;
        (*p)->height = max(((*p)->nodes[0],(*p)->nodes[1]))+1;
    }
    void SL(Node** p){

    }
    void RL(Node** p){

    }
    void LR(Node** p){

    }*/
    void replace_height(stack<Node**> all_nodes){
        /*for (iterin it=begin_in();it!=end_in();it++){
            balancear(&it.get_node());
            int altura1=-1,altura2=-1;
            if (it.get_node()->nodes[0])
                altura1 = it.get_node()->nodes[0]->altura;
            if (it.get_node()->nodes[1])
                altura2 = it.get_node()->nodes[1]->altura;
            it.get_node()->altura = (max(altura1,altura2)) +1;
            it.get_node()->fac_equi = altura1 - altura2;
        }*/
        int h1=-1,h2=-1;
        for(;!all_nodes.empty();all_nodes.pop()){
            balance(all_nodes.top());
            h1=-1,h2=-1;
            if ((*all_nodes.top())->nodes[0])
                h1 = (*all_nodes.top())->nodes[0]->height;
            if ((*all_nodes.top())->nodes[1])
                h2 = (*all_nodes.top())->nodes[1]->height;
            (*all_nodes.top())->height = (max(h1,h2)) +1;
            (*all_nodes.top())->fac_equi = h1 - h2;
        }

    }

//RL   SL LR  SR
    bool insert(T x){
        stack<Node**> all_nodes;
        Node** p;
        if (find(x,p,all_nodes)) return 0;
        (*p) = new Node(x);
        replace_height(all_nodes);
        return 1;
    }
    Node** replacement(Node** p){
        Node** tmp = &(*p)->nodes[0];
        while ((*tmp)->nodes[1])
            tmp = &(*tmp)->nodes[1];
        return tmp;
    }

    bool remove(T x){
        Node** p;
        stack<Node**> all_nodes;
        if (!find(x,p,all_nodes)) return 0;
        bool l=!!(*p)->nodes[0], r=!!(*p)->nodes[1];
        if (l & r){ //case 2
            Node** q = replacement(p);
            (*p)->data = (*q)->data;
            p=q;
            r=!!(*p)->nodes[1];
        }
        /*if (!l & !r){ //case 0
            delete *p;
            *p = 0;
            return 1;
        }*/
        //case 1
        Node* t = *p;
        *p = (*p)->nodes[r];
        delete t;
        t = 0;
        all_nodes.pop();
        replace_height(all_nodes);
        return 1;
    }
    void remove_node(Node* p){
        if (!p) return;
        remove_node(p->nodes[0]);
        delete (p);
        remove_node(p->nodes[1]);
    }

    ~CBinTree(){
        Node* p = root;
        remove_node(p);
    }
};

#endif // BINTREE_H
