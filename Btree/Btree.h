#ifndef BTREE_H
#define BTREE_H

#include <iostream>
#include "Node.h"

using namespace std;

template <class T>
class btree{
    typedef CNode<T> node;
    const bool overflow=0,nonfull=1;
public:
    node * root;
    btree () {
        root = new node();
    }
    bool search(T x){
        if (root->search_into_node(x)){
            return 1;
        }
        else{
            for (int i=0;i<root->length+1;i++)
                root->children[i]->search_into_node(x);
        }
    }
    bool insert(const T & x){
       /* if (search(x))
            return 0;*/
        bool of = insert2(root,x);
        if (of == overflow)
            splitRoot();
    }
    void print(node* ptr, int level) {
        if (ptr) {
            int i;
            for (i = ptr->length - 1; i >= 0; i--) {
               print(ptr->children[i+1], level + 1);
                for (int k = 0; k  < level; k++)
                    cout << "   ";
                cout << ptr->data [i] << endl ;
            }
            print( ptr->children[i+1], level + 1);
        }
    }
private:
    bool insert2(node* parent, const T& x){
        int pos = 0;
        while (pos< parent->length && parent->data[pos]< x)
            pos++;

        if (parent->children[pos]==NULL){
            parent->insert_into_node(pos,x);
        }
        else{
            bool of = insert2 (parent->children[pos],x);
            if (of == overflow){
                split (parent,pos);
            }
        }
        return (parent->length > m ) ? overflow : nonfull;
    }

    void splitRoot() {

        int i ;
        node* child = root ;
        node *node1 = new node();
        node *node2 = new node();
        for (i = 0; i < m/2; i++ ) {
            node1->data[i] = child->data[i];
            node1->children[i] = child->children[i];
            node1->length++;
        }
        node1->children[i] = child->children[i];
        i++;
        int k;
        for (k = 0;i <= m; k++, i++){
            node2->data[k] = child->data[i];
            node2->children[k] = child->children[i];
            node2->length++;

        }
        node2->children[k] = child->children[i];

        node* parent = new node();
        parent->insert_into_node(0,child->data[m/2]);
        parent->children[0] = node1;
        parent->children[1] = node2;
        root = parent;
    }

    void split(node *parent ,  int pos){
        int i;
        node* child = parent->children[pos] ;
        node *node1 = new node();
        node *node2 = new node();
        for (i = 0; i < m/2; i++) {
            node1->data[i] = child->data[i];
            node1->children[i] = child->children[i];
            node1->length++;
        }
        node1->children[i] = child->children[i];
        i++;
        int k;
        for (k =0 ; i <= m; k++, i++){
            node2->data[k] = child->data[i];
            node2->children[k] = child->children[i];
            node2->length++;

        }
        node2->children[k] = child->children[i];
        parent->insert_into_node (pos,child->data[m/2]);
        parent->children[pos] = node1;
        parent->children[pos+1] = node2;
    }

};

#endif // BTREE_H
