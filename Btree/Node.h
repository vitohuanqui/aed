#ifndef NODE_H
#define NODE_H

#define m 4

template<class T>
struct CNode{
    int length;
    T data[m+1];
    CNode* children[m+2];
    CNode(){
        length = 0;
        for (int i=0;i<m+2;i++)
            children[i]=NULL;
    }
    bool search_into_node(T x){
        for (int i=0;i<m;i++){
            if (data[i]==x)
                return 1;
        }
        return 0;
    }

    void insert_into_node(int pos, const T& x){
        /*
         red_black.insert(x);*/
       int i;
       for (i = m-1; i >= pos; i--) {
            this->data[i+1] = this->data[i];
            this->children[i+2] = this->children[i+1];
        }
        this->children[i+2] = this->children[i+1];
        this->data[pos] = x;
        this->length++;
    }
};
#endif // NODE_H
