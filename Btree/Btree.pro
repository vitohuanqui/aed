#-------------------------------------------------
#
# Project created by QtCreator 2014-11-19T15:19:45
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Btree
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    Node.h \
    Btree.h
