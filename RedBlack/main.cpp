#include <string>
#include <iostream>
using namespace std;
#ifndef datastructures_binary_tree_h
#define datastructures_binary_tree_h
#include <stack>

template<class T>
class binary_tree {
    struct node {
        T data;
        char color;
        node *left, *right, *parent;
        node (const T& _data) : data (_data) {
           left = right = parent = NULL;
           color = 'R';
        }
    };
    public:
    node* root;



    int  leftRotate( node *x )
    {
        //cout<<"rota izquierda"<<endl;
        node *y;
        if (x->right) {
            y = x->right;
            x->right = y->left;
            //cout<<y->right->data<<endl;
            if (y->right && y->left)
                y->left->parent = x;
            //cout<<"rota izquierda"<<endl;
            y->parent = x->parent;

            if ( x->parent == NULL )
                root = y;
            else
                if ( x == x->parent->left )
                     x->parent->left = y;
                else
                     x->parent->right = y;
            y->left = x;
            x->parent = y;

        }
        return 0;
    }


    int  rightRotate( node *x )
    {
        //cout<<"rota derecha"<<endl;
        node *y;
        if (x->left) {
            y = x->left;
            x->left = y->right;
            if (y->right)
                y->right->parent = x;
            y->parent = x->parent;
            if ( x->parent == NULL )
                root = y;
            else if ( x == x->parent->right )
                x->parent->right = y;
            else
                x->parent->left = y;
            y->right = x;
            x->parent = y;

        }
        return 0;
    }
    int promote( node *ptr )
    {
        node *tmp;

        while ( ptr->parent && ptr->parent->parent &&  padrer(ptr)==1 )
        {
            if(ptr->parent == ptr->parent->parent->left)
            {
                if(ptr == ptr->parent->left)
                {
                        cout<<"case 1"<<endl;
                        rightRotate(ptr->parent->parent);
                        ptr->color='B';
                        ptr->parent->right->color='B';
                        ptr->parent->color = 'R';
                        ptr=ptr->parent;

                }
                else
                {
                        cout<<"case 2"<<endl;
                        leftRotate(ptr->parent);
                        rightRotate(ptr->parent);
                        ptr->color='R';
                        ptr->left->color=ptr->right->color='B';
                }
            }
            else
            {
                if(ptr==ptr->parent->right)
                {
                        cout<<"case 3"<<endl;
                        //cout<<ptr->data<<endl;
                        //cout<<ptr->parent->parent->data<<endl;
                        leftRotate(ptr->parent->parent);
                        ptr->parent->color = 'R';
                        ptr->parent->left->color= 'B';
                        ptr->color= 'B';
                        ptr=ptr->parent;
                }
                else
                {
                        cout<<"case 4"<<endl;
                        rightRotate(ptr->parent);
                        leftRotate(ptr->parent);
                        ptr->color='R';
                        ptr->left->color = ptr->right->color = 'B';

                }
            }


        }

        root->color = 'B';
        return 0;
    }

public:
    binary_tree () {
        root = NULL;
    }
    int padrer (node * p)
    {
        if(p==root)
            return -1;
        else
            if(p->parent->color == 'R')
                return 1;
            else
                return 0;
    }

    void insert(const T & datas) {
        node* parenta = NULL;
        node** ptr = &root;
        if(*ptr==NULL)
        {
            *ptr = new node (datas);
            root->color= 'B';
            (*ptr)->parent = NULL;
        }
        else{
        while (*ptr != NULL) {
            parenta = *ptr;
            if(datas < (*ptr)->data )
                ptr = &((*ptr)->left);
            else
                ptr = &((*ptr)->right);

        }
        *ptr = new node (datas);
        (*ptr)->parent = parenta;}
        if(padrer(*ptr)==1)
            promote(*ptr);
    }


    void print () {
        print(root, 0);
        std::cout <<  std::endl;

    }

    void print (node* &ptr, int level) {
        if (ptr) {
            print (ptr->right, level + 1);
            for (int i = 0; i < level; i++) {
                std:: cout << "   ";
            }
            std::cout << ptr->data <<"-"<<ptr->color<< std::endl ;

            print (ptr->left, level + 1);

        }
    }

    public :
        class IteratorInor;
         void izqui ( node *&p)
        {
            while(p->left)
            {
                p=p->left;
            }
        }

        void dere ( node *& p)
        {
            while(p->right)
            {
                p=p->right;
            }
        }

        IteratorInor begin ()
        {
            node * p = root;
            izqui(p);
            return p;
        }

        IteratorInor end ()
        {
            node * p = root;
            dere(p);
            return p;
        }
        const IteratorInor begin () const
        {
            node * p = root;
            return izqui(p);
        }

        const IteratorInor end () const
        {
            node * p = root;
            dere(p);
            return p;
        }

        public :

        class IteratorInor
        {
            public:
            node * current ;
            stack <node *> path;

            void izqui ( node *& p)
            {//cout<<"EMPUJALO : ";
                path.push(p);
                while(p->left)
                {
                  path.push(p);
               //   cout<<p->data<<" ";
                  p=p->left;
                }
             //cout<<endl;
            }
            void impr ()
            {
                stack<node *> aux;
                aux =path;
                cout<<"PATH --> ";
                for(int i=0;i<=aux.size();i++)
                    {
                        cout<<(aux.top())->data<<"-";
                        aux.pop();
                    }
                cout<<endl;
            }

            IteratorInor(node * p){
                current = p;
                izqui(current);
                impr();
            };

            IteratorInor(const IteratorInor & it):current(it.current){};
            //IteratorInor(node * p):current(p){};
            const IteratorInor & operator = (const IteratorInor & it)
            {
                this->current = it.current;
                return *this;
            }

            bool operator == (const IteratorInor & it)
            {
                return current == it->current;
            }

            bool operator != (const IteratorInor & it)
            {
                return current != it.current;
            }

            T & operator * ()
            {
                return this->current->data;
            }

            IteratorInor & operator ++ (int)
            {
/*if(current == root)
                {
                    izqui(current);
                }*/
                //cout<<"PATH"<<endl;
                //impr();
                node * aux = path.top();
                current = aux;
                if(current->right)
                {
                    path.pop();
                    izqui(current->right);
                    return *this;
                }
                else{
                    path.pop();
                    return *this;
                }

            }

        };
};
#endif


int main()
{
    binary_tree<char> tree;
    string name = "ALGORITHM";
    for (int i = 0; i  < name.size(); i++) {
        tree.insert ( name [i] );
        tree.print();
    }


    binary_tree<char>::IteratorInor it(tree.root);
    binary_tree<char>::IteratorInor jt(tree.root);
    it = tree.begin();
    jt = tree.end();
    for(;it!=jt;it++)
        cout<<*it<<"-"<<endl;
    //it = tree.end();

    /*stack<int> a;
    a.push(7);
    a.push(6);
    for(int i=0;i<=a.size();i++)
    {
        cout<<a.top()<<"-";
        a.pop();
    }*/

    return 0;
}
