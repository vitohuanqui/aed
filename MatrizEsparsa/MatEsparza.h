#ifndef MATESPARZA_H
#define MATESPARZA_H
#include "CNode.h"
#include "MatEsparza.h"
#include <iostream>

using namespace std;

template<class T,unsigned int N>
class MatEsp{
public:
    typedef CNode<T> Node;
    typedef MatEsp<T,N> self;
    Node* vertical[N];
    Node* horizontal[N];
    MatEsp(){
        for (int i=0;i<N;i++){
            vertical[i] = horizontal[i] = NULL;
        }
    }

    T& operator ()(int x,int y){
        if (!horizontal[y] && !vertical[x]){
            Node* to_create;
            to_create = new Node(x,y);
            vertical[x] = to_create;
            horizontal[y] = to_create;
            return to_create->data;
        }
        else if (horizontal[y] && !vertical[x]){
            Node* to_create;
            to_create = new Node(x,y);
            Node** tmpy = &horizontal[y];
            for(;(*tmpy)->abajo && (*tmpy)->abajo->x<x;tmpy=&(*tmpy)->abajo);
            if ((*tmpy)->x>x){
                to_create->abajo = horizontal[y];
                horizontal[y] = to_create;
            }
            else{
                to_create->abajo = (*tmpy)->abajo;
                (*tmpy)->abajo = to_create;
            }
            vertical[x] = to_create;
            return to_create->data;
        }
        else if (!horizontal[y] && vertical[x]){
            Node* to_create;
            to_create = new Node(x,y);
            Node** tmpx = &vertical[x];
            for(;(*tmpx)->sig && (*tmpx)->sig->y<y;tmpx=&(*tmpx)->sig);
            if ((*tmpx)->y>y){
                to_create->sig = vertical[x];
                vertical[x] = to_create;
            }
            else{
                to_create->sig = (*tmpx)->sig;
                (*tmpx)->sig = to_create;
            }
            horizontal[y] = to_create;
            return to_create->data;
        }
        else{
            Node* to_create;
            to_create = new Node(x,y);
            Node** tmpx = &vertical[x];
            Node** tmpy = &horizontal[y];
            for(;(*tmpx)->sig && (*tmpx)->sig->y<y;tmpx=&(*tmpx)->sig);
            for(;(*tmpy)->abajo && (*tmpy)->abajo->x<x;tmpy=&(*tmpy)->abajo);
            if ((*tmpx)->y==y && (*tmpy)->x==x){
                return (*tmpx)->data;
            }
            if ((*tmpx)->y>y){
                to_create->sig = vertical[x];
                vertical[x] = to_create;
            }
            else{
                to_create->sig = (*tmpx)->sig;
                (*tmpx)->sig = to_create;
            }
            if ((*tmpy)->x>x){
                to_create->abajo = horizontal[y];
                horizontal[y] = to_create;
            }
            else{
                to_create->abajo = (*tmpy)->abajo;
                (*tmpy)->abajo = to_create;
            }
            return to_create->data;
        }
    }
    Node*& operator =(const T x){

        cout<<"entro2<insercion de dato "<<endl;
        this->Node.data = x;
        return this;
    }
};

ostream& operator << (ostream &o,MatEsp<int,4> &p)
{
    o<< "  " ;
    MatEsp<int,4>::Node** tmpx;
    for (int i=0;i<4;i++){
        o<< i ;
    }
    o<<endl;
    for (int i=0;i<4;i++){
        o<<i<<" ";
        if (p.vertical[i]){
            tmpx = &p.vertical[i];
            for(int j=0;j<4;j++){
                if ((*tmpx)->y==j){
                    o << (*tmpx)->data;
                    (tmpx) = &(*tmpx)->sig;
                }
                else{
                    o << "-";
                }
            }
        }
        else
            o<<"----";
        o<<endl;
    }
    return o;
}

#endif // MATESPARZA_H
