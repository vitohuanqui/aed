#ifndef CNODE_H
#define CNODE_H

template<class T>
struct CNode{
    CNode(int x_,int y_,T data_){
        x=x_;
        y=y_;
        data=data_;
    }
    CNode(int x_,int y_){
        x=x_;
        y=y_;
        sig = abajo = NULL;
    }
    ~CNode(){}
    int x;
    int y;
    T data;
    CNode<T>* sig;
    CNode<T>* abajo;
};

template<class T>
struct aux{
    aux(){}
    ~aux(){}


};

#endif // CNODE_H
