#-------------------------------------------------
#
# Project created by QtCreator 2014-11-26T15:27:06
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = MatrizEsparsa
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    CNode.h \
    MatEsparza.h
