#ifndef TRAIT_H
#define TRAIT_H

#include "Routes.h"

using namespace std;

template <typename tr>
struct InOrder
{
    typedef typename tr::data_type data_type;
    typedef InOr<data_type> iterator_move;
};

template <typename tr>
struct PostOrder
{
    typedef typename tr::data_type data_type;
    typedef PostOr<data_type> iterator_move;
};

template <typename tr>
struct PreOrder
{
    typedef typename tr::data_type data_type;
    typedef PreOr<data_type> iterator_move;
};

template <class T>
class More
{
    public:
        More() {}
        virtual ~More() {}

        int operator()(const T &x , const T &y)
        {
            if(x<y)     return -1;
            if(x>y)     return 1;
            return 0;
        }
    protected:
    private:
};

template <typename T>
struct AscTreeTrait
{
    typedef T data_type;
    typedef More<data_type> cmp_type;
};

#endif // TRAIT_H
