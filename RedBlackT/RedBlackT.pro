#-------------------------------------------------
#
# Project created by QtCreator 2014-10-22T20:56:28
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = RedBlackT
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    Iterator.h \
    CNode.h \
    Trait.h \
    Routes.h \
    RedBlackTree.h
