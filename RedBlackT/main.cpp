#include <iostream>
#include "RedBlackTree.h"
#include "Trait.h"

using namespace std;

int main(int argc, char *argv[])
{
   // QCoreApplication a(argc, argv);
    RedBlackTree<AscTreeTrait<int> > arbol;
    for (int i = 0; i < 100; ++i)
    {
        arbol.add(i);
    }
    cout<<endl<<endl;
    arbol.print();
    arbol.remove(19);
    cout<<endl<<endl;
    arbol.print();
    //return a.exec();
}
