#-------------------------------------------------
#
# Project created by QtCreator 2014-11-25T10:55:16
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Digital
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    CNode.h \
    DigitalTree.h
