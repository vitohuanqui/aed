#ifndef DIGITALTREE_H
#define DIGITALTREE_H

#include "CNode.h"

template<class T>
class DigitalTree{
public:
    typedef CNode<T> Node;
    Node* root;
    bool find(string &x, Node**& p, int& pos){
        if ((*p)==NULL) return 0;
        int tam,acum=0;
        if ((*p)->data.size()<=x.size()-pos)
            tam = (*p)->data.size();
        else
            tam = x.size()-pos;
        for (int i=0;i<tam;i++){
            if ((*p)->data[i]==x[pos+i])
                acum++;
            else
                break;
        }
        if ((acum==x.size()-pos)&&(x.size()-pos==(*p)->data.size()))return 1;

        else if ((*p)->data.size()==acum){
            pos+=acum;
            x=x.substr(pos,x.size());
            pos=0;
            p=&(*p)->sub_tree;
            find(x,p,pos);
        }
        else{
            if (acum==0){
                p=&(*p)->m_nodes[((*p)->data[0]<x[pos])];
                pos+=acum;
                find(x,p,pos);
            }
            else{
                pos+=acum;
                x.substr(pos,x.size());
                return 0;
            }
        }/*
            find(x.substr(1,x.end()),(*p)->m_nodes[((*p)->data[0]<x[0])])

        if (root->data==x.substr(0,root->data.size()))
            find(x.substr(root->data.size(),x.end()));// modificar
        else{
            if (root->data>x.substr(0,root->data.size()))


        }*/
    }
public:
    DigitalTree(){
        root = NULL;
    }
    bool find1(string x){
        Node** tmp = &root;
        int pos;
        return find(x,tmp,pos);
    }

    bool insert(string x){
        Node** tmp = &root;
        int pos=0;
        string x_toi = x;
        if (find(x_toi,tmp,pos)) return 0;
        if (pos==0){
            (*tmp) = new Node(x_toi);
            return 1;
        }
        else{
            string tmp_s;
            if (!(*tmp)){
                *tmp = new Node(x_toi.substr(pos,x_toi.size()));
            }
            else if (!(*tmp)->sub_tree){
                tmp_s = (*tmp)->data;
                (*tmp)->data = tmp_s.substr(0,pos);
                (*tmp)->sub_tree = new Node();
                (*tmp)->sub_tree->data = tmp_s.substr(pos,tmp_s.size());
                (*tmp)->sub_tree->m_nodes[x_toi[pos]>tmp_s[pos]] = new Node();
                (*tmp)->sub_tree->m_nodes[x_toi[pos]>tmp_s[pos]]->data=x_toi.substr(pos,x_toi.size());
            }
            else{
                /*cout<<"entro 3 "<<x<<endl;
                (*tmp)->sub_tree->m_nodes[x[pos]>tmp_s[pos]] = new Node();
                (*tmp)->sub_tree->m_nodes[x[pos]>tmp_s[pos]]->data=x.substr(pos,x.size());*/
            }
            return 1;
        }
        return 0;
    }

    bool remove1(string x){
        Node** tmp = &root;
        string x_=x;
        return remove(x_,tmp);
    }
    bool remove(string &x, Node**& p){
        if ((*p)==NULL)
            return 0;
        if ((*p)->data==x.substr(0,(*p)->data.size())){
            if ((*p)->data.size()==x.size()){
                if (!(*p)->sub_tree){
                    delete (*p);
                    (*p)=NULL;
                    return 1;
                }
                else{
                    ((*p)->data) = ((*p)->data)+((*p)->sub_tree->data);
                    if ((*p)->sub_tree){
                        (*p)->m_nodes[0] = (*p)->sub_tree->m_nodes[0];
                        (*p)->m_nodes[1] = (*p)->sub_tree->m_nodes[1];
                        (*p)->sub_tree = (*p)->sub_tree->sub_tree;
                    }
                    else {
                        delete (*p)->sub_tree;
                        (*p)->sub_tree = NULL;
                    }
                }
            }
            else{
                x=x.substr((*p)->data.size(),x.size());
                p=&(*p)->sub_tree;
                return remove(x,p);
            }
        }
        else{
            p=&((*p)->m_nodes[x[0]>(*p)->data[0]]);
            return remove(x,p);
        }
    }
};

#endif // DIGITALTREE_H
