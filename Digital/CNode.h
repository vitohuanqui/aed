#ifndef CNODE_H
#define CNODE_H

#include<iostream>
#include<sstream>
#include<fstream>

using namespace std;

template<class T>
struct CNode{
    string data;
    CNode<T>* m_nodes[2];
    CNode<T>* sub_tree=NULL;
    CNode(){
        m_nodes[0]=m_nodes[1]=sub_tree=NULL;
    }
    CNode(T x){
        stringstream f;
        f<<x;
        f>>data;
    }
};

#endif // CNODE_H
