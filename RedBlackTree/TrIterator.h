#ifndef TRITERATOR_H
#define TRITERATOR_H

#include "Routes.h"

using namespace std;

template <typename tr>
struct InOrder
{
    typedef typename tr::data_type data_type;
    typedef InOr<data_type> iterator_move;
};

template <typename tr>
struct PostOrder
{
    typedef typename tr::data_type data_type;
    typedef PostOr<data_type> iterator_move;
};

template <typename tr>
struct PreOrder
{
    typedef typename tr::data_type data_type;
    typedef PreOr<data_type> iterator_move;
};


#endif // TRITERATOR_H
