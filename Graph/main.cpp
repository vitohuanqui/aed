#include <iostream>
#include <list>
#include <vector>
using namespace std;
template<class G>
class CNode
{
    typedef typename G::E E;
    typedef typename G::Edge Edge;
    typedef typename G::lista lst;
public:
    vector<Edge*> m_edges;
    E m_data;
    CNode(int x){
        m_data=x;
    }
    ~CNode();
};

template<class G>
class CEdge
{
    typedef typename G::N N;
    typedef typename G::Node Node;
public:
    int m_data;
    Node* m_nodes[2];
    bool m_dir; //0 = bidirecional  1= de origen a destino
    CEdge(Node* a, Node* b, int x, bool dir){
        m_data=x;
        m_nodes[0]=a;
        m_nodes[1]=b;
        m_dir=dir;
    }
    ~CEdge(){

    }
};

template<class A,class B>
class CGraph
{
public:
    typedef CGraph<A,B> self;
    typedef CNode<self> Node;
    typedef CEdge<self> Edge;
    typedef list<Edge*> lista;
    typedef A N;
    typedef B E;
    CGraph(){};
    ~CGraph(){};
    vector<Node*> m_nodes;
    bool insertar_Node(N x){
        Node* tmp = new Node(x);
        m_nodes.push_back(tmp);
    }
    bool insertar_arista(Node* a, Node* b, E x, bool dir){
        Edge* ar;
        if (dir){
            ar=new Edge(a,b,x,dir);
            a->m_edges.push_back(ar);
        }
        else{
            ar=new Edge(a,b,x,dir);
            a->m_edges.push_back(ar);
            ar=new Edge(b,a,x,dir);
            b->m_edges.push_back(ar);
        }
    }
    bool search(N x,int& pos){
        for (int i=0;i<m_nodes.size();i++){
            if (m_nodes[i]->m_data==x){
                pos=i;
                return 1;
            }
        }
        return 0;
    }
    bool delete_edge(Edge* x){
        x->m_nodes[0]->m_edges.remove(x);
        x->m_nodes[1]->m_edges.remove(x);
    }
    bool delete_Node(N x){
        int pos;
        int tam;
        if(!search(x,pos))
            return 0;
        else{
            tam=m_nodes[pos]->m_edges.size();
            for (int i=0;i<tam;i++){
                if (m_nodes[pos]->m_edges[i]->m_dir)
                    delete_edge(m_nodes[pos],m_nodes[pos]->m_edges[i]->m_data);
                else
                    delete_edge(m_nodes[pos],m_nodes[pos]->m_edges[i]->m_nodes[1],m_nodes[pos]->m_edges[i]->m_data);
            }
            m_nodes.erase(m_nodes.begin()+pos);
        }
    }
    bool find_edge(Node* a, E x, int& q, Edge ** p){
        for(int j=0;j<a->m_edges.size();j++){
            if (a->m_edges[j]->m_data==x){
                q=j;
                p=&a->m_edges[j];
                return 1;
            }
        }
        return 0;
    }

    bool delete_edge(Node* a, Node *b, E x){
        int q;
        Edge** p;
        if (!find_edge(a,x,q,p)) return 0;
        a->m_edges.erase(a->m_edges.begin()+q);
        if (!find_edge(b,x,q,p)) return 0;
        b->m_edges.erase(b->m_edges.begin()+q);
        return 1;
    }

    bool delete_edge(Node* a, E x){
        int q;
        Edge** p;
        if (!find_edge(a,x,q,p)) return 0;
        a->m_edges.erase(a->m_edges.begin()+q);
        delete *p;
    }
    void print(){
        for (int i=0;i<m_nodes.size();i++){

            cout<<m_nodes[i]->m_data;
            cout<<" aristas ";
            for(int j=0;j<m_nodes[i]->m_edges.size();j++){
                if (m_nodes[i]->m_edges[j]!=NULL){
                cout<<m_nodes[i]->m_edges[j]->m_nodes[1]->m_data<<" - ";
                }
            }
            cout<<endl;
        }
    }
};

int main(){
    CGraph<int,int> grafo;
    grafo.insertar_Node(5);
    grafo.insertar_Node(4);
    grafo.insertar_Node(3);
    grafo.insertar_arista(grafo.m_nodes[0],grafo.m_nodes[1],4,0);
    grafo.insertar_arista(grafo.m_nodes[0],grafo.m_nodes[2],4,0);
    grafo.print();
    grafo.delete_Node(5);
    grafo.print();
}
